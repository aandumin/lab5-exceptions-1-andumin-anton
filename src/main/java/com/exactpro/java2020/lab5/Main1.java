package com.exactpro.java2020.lab5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Main1 {
    public static void main(String[] args) {
        Main1 app = new Main1();
        Set<Exception> exceptions = app.get10Exception();

        System.out.println(exceptions);
    }

    public Set<Exception> get10Exception() {
        Set<Exception> exceptions = new HashSet<>();

        try {
            int i = 1 / 0;
        } catch (ArithmeticException e) {
            exceptions.add(e);
        }

        try {
            int[] a = new int[1];
            int b = a[1];
        } catch (ArrayIndexOutOfBoundsException e) {
            exceptions.add(e);
        }

        try {
            String s = null;
            s.length();
        } catch (NullPointerException e) {
            exceptions.add(e);
        }

        try {
            String s = "qwerty";
            int i = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            exceptions.add(e);
        }

        try {
            int[] a = new int[-1];
        } catch (NegativeArraySizeException e) {
            exceptions.add(e);
        }

        try {
            BufferedReader br = new BufferedReader(new FileReader("noSuchFile"));
            //String s =
        } catch (IOException e) {
            exceptions.add(e);
        }

        try {
            String s = "qwerty";
            char c = s.charAt(7);
        } catch (StringIndexOutOfBoundsException e) {
            exceptions.add(e);
        }

        try {
            URL url = new URL("myURL");
            URI uri = new URI(url.getProtocol());
        } catch (MalformedURLException e) {
            exceptions.add(e);
        } catch (URISyntaxException e) {
            exceptions.add(e);
        }

        try {
            String dateStr = "2011 11 19";
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse(dateStr);
        } catch (ParseException e) {
            exceptions.add(e);
        }

        try {
            throw new NoRouteToHostException("test");
        } catch (NoRouteToHostException e) {
            exceptions.add(e);
        }

        return exceptions;
    }
}
