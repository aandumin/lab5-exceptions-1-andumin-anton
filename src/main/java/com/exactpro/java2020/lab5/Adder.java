package com.exactpro.java2020.lab5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Adder {
    /**
     * Читает входной файл. Возвращает сумму чисел из файла. Пропускает некорректные значения в файле
     * и выводит их в System.err.
     * <p>
     * Если файла не существует, то выводит форматированное сообщение в System.err:
     * <b><code>("Input file with numbers not found: %s%n", path)</code></b>.
     * <p>
     * Если возникла другая ошибка при чтении файла, то выводит форматированное сообщение в System.err:
     * <b><code>("An unexpected error occurred while reading the file '%s'%n", path)</code></b> и stackTrace.
     *
     * @param path путь до входного файла
     * @throws ArithmeticException ошибка, если сумма чисел меньше нуля
     */
    public void processFile(String path) throws ArithmeticException {
        List<Float> numbers = new ArrayList<Float>();

        if (path == null) {
            System.err.format("An unexpected error occurred while reading the file '%s'%n", path);
        } else {
            File file = new File(path);
            if (!file.exists()) {
                System.err.format("Input file with numbers not found: %s%n", path);
            } else {
                try {
                    numbers = readNumbers(path);
                } catch (IOException e) {
                    //e.printStackTrace();
                    System.err.format("An unexpected error occurred while reading the file '%s'%n", path);
                }
                float sum = calculate(numbers);
                System.out.println("Sum: " + sum);
            }
        }
    }


    /**
     * Читает входной файл. Возвращает список чисел. Пропускает некорректные значения и выводит их в System.err
     * с помощью метода {@link Adder#printUnparsedLinesWarning}.
     *
     * @param path путь до входного файла
     * @return список чисел
     * @throws IOException ошибка при чтении файла
     */
    public List<Float> readNumbers(String path) throws IOException {
        List<Float> numbers = new ArrayList<Float>();
        List<String> unparsedRows = new ArrayList<>();
        String line = "";

        List<String> rows = readFile(path);
        for (int i = 0; i < rows.size(); i++) {
            line = rows.get(i);
            if (line.matches("(-?[0-9])")) {
                numbers.add(Float.parseFloat(line));
            } else {
                unparsedRows.add(line);
            }
        }

        printUnparsedLinesWarning(unparsedRows);
        return numbers;
    }

    /**
     * Читает входной файл. Возвращает список строк.
     *
     * @param path путь до входного файла
     * @return список строк
     * @throws IOException ошибка при чтении файла
     */
    public List<String> readFile(String path) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader(path));
        List<String> rows = new ArrayList<String>();
        if (path == null || !br.ready()) {
            throw new IOException("An error has occurred while file reading");
        }
        String line;
        while ((line = br.readLine()) != null) {
            rows.add(line);
        }
        br.close();
        return rows;
    }

    /**
     * Выводит в System.err строку вида <b><code>Unparsed lines: "a", "", "bc".</code></b>.
     *
     * @param unparsedLines коллекия строк для вывода
     */
    public void printUnparsedLinesWarning(List<String> unparsedLines) {
        StringBuilder sb = new StringBuilder();
        char ch = '"';
        for (int i = 0; i < unparsedLines.size(); i++) {
            sb.append('"');
            sb.append(unparsedLines.get(i));
            sb.append('"');
            if (i < unparsedLines.size() - 1) {
                sb.append(", ");
            } else {
                sb.append(".");
            }
        }
        String lineToPrint = sb.toString();
        if (unparsedLines.size() > 0) {
            System.err.format("Unparsed lines: %s%n", lineToPrint);
        }
    }

    /**
     * Подсчет суммы входных чисел.
     *
     * @param numbers список чисел для суммированмя
     * @return число - сумма входных чисел
     * @throws ArithmeticException ошибка вида <b><code>"The sum of numbers less than zero: " + sum</code></b>
     */
    public float calculate(List<Float> numbers) throws ArithmeticException {
        float sum = 0;
        for (int i = 0; i < numbers.size(); i++) {
            sum = sum + numbers.get(i);
        }
        if (sum < 0) {
            throw new ArithmeticException("The sum of numbers less than zero: " + sum);
        }
        return sum;
    }
}

