package com.exactpro.java2020.lab5;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AdderTest {
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errorStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
        System.setErr(new PrintStream(errorStreamCaptor));
    }

    // All app tests

    @Test
    public void inputFile1Test() {
        Adder adder = new Adder();
        adder.processFile("src/test/resources/input-1.txt");
        assertEquals("Sum: 4.0", outputStreamCaptor.toString().trim());
        assertEquals("", errorStreamCaptor.toString().trim());
    }

    @Test
    public void inputFile2Test() {
        Adder adder = new Adder();
        adder.processFile("src/test/resources/input-2.txt");
        assertEquals("Sum: 6.0", outputStreamCaptor.toString().trim());
        assertEquals("Unparsed lines: \"dva\", \"\", \"4etyre\".", errorStreamCaptor.toString().trim());
    }

    @Test
    public void inputFile3Test() {
        Adder adder = new Adder();
        Exception exception = assertThrows(ArithmeticException.class, () -> {
            adder.processFile("src/test/resources/input-3.txt");
        });
        String expectedMessage = "The sum of numbers less than zero: -4.0";
        String actualMessage = exception.getMessage();

        assertEquals("", outputStreamCaptor.toString().trim());
        assertEquals("", errorStreamCaptor.toString().trim());
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void nonExistingInputFileTest() {
        Adder adder = new Adder();
        adder.processFile("empty");

        assertEquals("", outputStreamCaptor.toString().trim());
        assertEquals("Input file with numbers not found: empty", errorStreamCaptor.toString().trim());
    }

    @Test
    public void NullInputFileTest() {
        Adder adder = new Adder();
        adder.processFile(null);

        assertEquals("", outputStreamCaptor.toString().trim());
        assertTrue(errorStreamCaptor.toString().trim().startsWith("An unexpected error occurred while reading the file 'null'"));
    }
}