package com.exactpro.java2020.lab5;

import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

public class Main1Test {
    @Test
    public void testGet10Exceptions() {
        Main1 app = new Main1();
        Set<Exception> exceptions = app.get10Exception();
        Assert.assertTrue(exceptions.size() >= 10);
    }
}